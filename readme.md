# HyprDots

HyprDots are minimal config for hyprland on Arch linux.

# Switched to Hyprpanel
As of 25-dec-2024, hyprdots have migrated to hyprpanel in lieu of waybar. Run the following commands to install hyprpanel. 

```
yay -Sy ags-hyprpanel-git
yay -S --noconfirm -<.hyprpanel.txt

```


# WARNING
 Hyprland is not suitable to be run on VM. You might have a subpar experience in doing so. 

# Screenshots

![Screenshot 1](.screenshots/1.jpg?raw=true "Screenshot 1")

![Screenshot 2](.screenshots/2.jpg?raw=true "Screenshot 2")

![Screenshot 3](.screenshots/3.jpg?raw=true "Screenshot 3")

![Screenshot 4](.screenshots/4.jpg?raw=true "Screenshot 4")

![Screenshot 5](.screenshots/5.jpg?raw=true "Screenshot 5")

![Screenshot 6](.screenshots/6.jpg?raw=true "Screenshot 6")

![Screenshot 7](.screenshots/7.jpg?raw=true "Screenshot 7")

![Screenshot 8](.screenshots/8.jpg?raw=true "Screenshot 8")

![Screenshot 9](.screenshots/9.jpg?raw=true "Screenshot 9")

![Screenshot 10](.screenshots/10.jpg?raw=true "Screenshot 10")

# Required Packages

The required packages are listed in `.requirements.txt` these packages are extremely important for the basic functionalities of hyprland Please do not remove any of them

# User Packages

The user packages are listed in `.packages`. Feel free to add or remove any packages from here as you please.

# Manual Installation

#### 1. Install yay

```
sudo pacman -Sy --needed git base-devel
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
```

#### 2. Clone this Repo in your home directory:

```
cd
git clone https://gitlab.com/paridhips/hyprdots.git
```

#### 3. Update & Install required packages
- Update Your System and reboot

```
sudo pacman -Syu
reboot
```
- Change your present working directory
```
cd hyprdots
```

- Install the required packages
```
yay -S --noconfirm -<.requirements.txt
```

#### 4. Use GNU Stow to symlink the dot files:
- Run the following commands.
```
cd ~/hyprdots
stow --adopt .
git reset --hard 
```

#### 5. Change Shell to zsh

```
chsh -s $(which zsh)
touch .zsh_history
```

#### 6. Install user packages

```
yay -S --noconfirm -<.packages.txt
```

# Theming

Theming is done by matugen. However setting the wallpaper has to be done by hyprpanel for the colour scheme to take effect in both hyprpanel and other applications. 
The necessary templates for matugen to work are in the repo.
Set the default theme as adwaita
```
gsettings set org.gnome.desktop.interface gtk-theme 'adw-gtk3'
```
To set a new wallpaper run
```
 hyprpanel setwallpaper <path to wallpaper>
```

# Librewolf theming
Libre wolf can be themed using pywalfox. The dependency is already present in `.requirements.txt`.
To install pywalfox run
```
pywalfox install --browser librewolf
```

# Discord theming
Discord theming is done throught [Better Discord](https://betterdiscord.app/)
Go to settings -> Themes (under better discord) -> turn on 'midnight'

When discord is updated, you have to reinstall betterdiscord. Run the command below
```
betterdiscordctl install
```

# Auto Install Script
There are two list of packages 
| File Name   | Description       |
|-------------|-------------------|
| .requirements.txt | Mandatory list of packages that are required for the configs to work|
| .packages.txt | list of packages that I personally use for my day to day activities|

Do not run the script without reading through it first. The script is not interactive. So make sure to go trough it before running it. 

Also please remove or add any packages as per your needs. 



