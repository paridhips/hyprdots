echo "Installing git"
sudo pacman -Sy --noconfirm git

# Pulling the Repo
echo "clone the repo"
git clone https://gitlab.com/paridhips/hyprdots.git


# Installing Yay
echo "Hello there! Installing yay now! "
sudo pacman -Sy --needed git base-devel
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

# Installing Packages
cd ~/hyprdots
echo "Installing Required Packages."
yay -S  --noconfirm -<.requirements.txt
yay -S --noconfirm -<.packages.txt

# Symlink dots
stow --adopt .


# Post Install
rm -rf yay

# Copy Wallpaper
cd ~/Pictures/
git clone https://gitlab.com/sapphic-sword-lover/wallpaper.git

# Create Directories
mkdir ~/Pictures/screenshot

# Enable Bluetooth
sudo systemctl start bluetooth.service
sudo systemctl enable bluetooth.service
# Git config
git config --global user.name "Paridhi Prisha Saikia"
git config --global user.email "paridhiprishasaikia@gmail.com"

# Better Discord 
curl -O https://raw.githubusercontent.com/bb010g/betterdiscordctl/master/betterdiscordctl
chmod +x betterdiscordctl
sudo mv betterdiscordctl /usr/local/bin
betterdiscordctl install

# Pywalfox
pywalfox install --browser librewolf

# Change Shell
echo "Switching to zsh! Meow"
cd
chsh -s $(which zsh)
touch .zsh_history
#Set theme
gsettings set org.gnome.desktop.interface gtk-theme 'adw-gtk3'
# KVM
sudo pacman -S virt-manager qemu vde2 ebtables dnsmasq bridge-utils openbsd-netcat
sudo systemctl enable libvirtd.service
sudo systemctl start libvirtd.service
sudo usermod -a -G libvirt $(whoami)
