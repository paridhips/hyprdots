#!/bin/bash
if [ -z "$1" ]; then
    echo "Please provide the path to the image."
    exit 1
fi

# Assign the argument to a variable
IMAGE_PATH="$1"

# Run matugen and waypaper commands with the provided path
matugen image "$IMAGE_PATH" -m dark
waypaper --wallpaper "$IMAGE_PATH"
pkill -SIGUSR2 waybar
