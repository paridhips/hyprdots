vim.cmd("set expandtab")
vim.cmd("set tabstop=2")
vim.cmd("set softtabstop=2")
vim.cmd("set shiftwidth=2")
vim.cmd("set backupcopy=yes")
vim.o.cursorline=true
vim.o.cursorcolumn=true
vim.wo.number=true
vim.g.mapleader=" "


