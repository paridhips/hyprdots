return {
    "nvim-treesitter/nvim-treesitter", build=":TSUpdate",
    config = function()
        local config=require("nvim-treesitter.configs")
        config.setup({
            lazy = false,
            version = nil,
            auto_install =true,
            ensure_installed={"java","javascript"},
            highlight={ enable =true},
            indent={ enable=true },
            sync_install = false,
            })
    end
  }
 
